# Visualising emotions over a time period
An example of using the NXC sentiment API to see how Twitter users are feeling about a project over given time period.   
**Supported emotions:**
- sadness
- joy 
- fear  
- love  
- anger  
- optimism  
- surprise  
